#ifndef OBSTACLE_H
#define OBSTACLE_H

#include "coordinate.h"

class obstacle{
    public:
        obstacle();
        ~obstacle();

        void renderObstacle();
        void moveObstacle();
        void removeObstacle();
        void addNonObstacle();
        void drawObstacle();

        coordinate c1;
        coordinate c2;
        coordinate c3;
        coordinate c4;

        int weight;
    protected:

    private:


};

#endif
