#ifndef OPENING_H
#define OPENING_H

#include <windows.h>
#include <GL/glut.h>
#include <cstdlib>

#include "text.h"


class opening {
    public:
        opening();
        ~opening();

        static void renderMainMenu();
        static void renderCharacterSelector();

    protected:

    private:
};

#endif // BACKGROUND_H
