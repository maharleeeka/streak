#ifndef COORDINATE_H
#define COORDINATE_H


class coordinate{
    public:
        coordinate();
        ~coordinate();

        static void setCoordinate();
        void setCoordinate(float x, float y);
        coordinate getCoordinate();
        static float getCoordinateX();
        static float getCoordinateY();

        float x;
        float y;

    protected:

    private:

};

#endif
