#ifndef BACKGROUND_H
#define BACKGROUND_H


class background {
    public:
        background();
        ~background();

        void renderUpperPlatform();
        void renderLowerPlatform();
    protected:

    private:
};

#endif // COLLISION_H
