#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <windows.h>
#include <GL/glut.h>
#include <cstdlib>
#include <iostream>

#include "player.h"
#include "shapes.h"
#include "obstacle.h"

class controller
{
    public:
        controller();
        ~controller();

        void bg();
        void changeBG();
        void gameStart();
        void movePlayer(int);
        player Player;
        obstacle Obstacle;
        void showObstacles();
        void checkCollision();
        bool checkFloatEquality(float, float);
        bool gamePlay;

    protected:

    private:
        int life;
};

#endif // CONTROLLER_H
