#ifndef PLAYER_H
#define PLAYER_H

#include <windows.h>
#include <GL/glut.h>

#include "coordinate.h"

class player{
    public:
        player();
        ~player();

        void renderPlayer();
        void playerShape();
        void setLife(float);

        float getPlayerPosX();
        float getPlayerPosY();
        void checkCollision();
        void setPlayerPos(coordinate, coordinate, coordinate, coordinate);
        void setPlayerPosX(float);
        void setPlayerPosY(float);

        coordinate c1;
        coordinate c2;
        coordinate c3;
        coordinate c4;

        int capacity;
        float basket;
    protected:

    private:
        int life;
        int streak;

};

#endif // PLAYER_H
