#include "background.h"
#include "shapes.h"
#include "coordinate.h"

background::background(){

}

background::~background(){

}

void background::renderUpperPlatform(){
    float x = -0.9;
    float y = -0.7;

    coordinate c1, c2, c3, c4;
    c1.x = x;
    c1.y = y;
    c2.x = x;
    c2.y = y + -0.2;
    c3.x = x * -1;
    c3.y = y + -0.2;
    c4.x = x* -1;
    c4.y = y;
    glColor3f(0.7f, 0.7f, 0.8f);
    shapes::drawPolygon1(c1, c2, c3, c4);
}

void background::renderLowerPlatform(){
    float x = -0.9;
    float y = 0.9;

    coordinate c1, c2, c3, c4;
    c1.x = x;
    c1.y = y;
    c2.x = x;
    c2.y = y - 0.2;
    c3.x = x * -1;
    c3.y = y -0.2;
    c4.x = x * -1;
    c4.y = y;
    glColor3f(0.7f, 0.7f, 0.8f);
    shapes::drawPolygon1(c1, c2, c3, c4);
}
