#include "controller.h"
#include "background.h"
using namespace std;

background Background;

controller::controller(){
    gamePlay = true;
}

controller::~controller(){

}

void controller::bg(){
        changeBG();
}

void controller::changeBG(){
    int star_count = rand() % 50 + 1;

    glPushMatrix();
        glBegin(GL_POINT);
            for(;star_count > 0; star_count--)
            {
                float rx = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
                float ry = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);

                if(rand() % 2 == 0)
                {
                    rx = rx * -1;
                }

                if(rand() % 2== 0)
                {
                    ry = ry * -1;
                }

                if(star_count % 2 == 0)
                    glColor3f(1, 1, 1);
                else
                    glColor3f(1, 1, 0);
                shapes::drawColoredCircle(rx, ry, 361, 0.003, 0.003);

                //std::cout<<rx<<" "<<ry<<std::endl;
            }
        glEnd();
    glPopMatrix();
}

void controller::gameStart(){
    Background.renderLowerPlatform();
    Background.renderUpperPlatform();
    Obstacle.renderObstacle();
    if(gamePlay){
        Player.renderPlayer();
    }
}

void controller::movePlayer(int direction) {
    if (direction == 1) {
        //left
        Player.c1.x -= 0.1;
        Player.c2.x -= 0.1;
        Player.c3.x -= 0.1;
        Player.c4.x -= 0.1;
        if (Player.c1.x <= -0.9 || Player.c2.x <= -0.9) {
            Player.c1.x = -0.9;
            Player.c2.x = -0.9;
            Player.c3.x = -0.7;
            Player.c4.x = -0.7;
        }
    } else if (direction == 2) {
        //right
        Player.c1.x += 0.1;
        Player.c2.x += 0.1;
        Player.c3.x += 0.1;
        Player.c4.x += 0.1;
        if (Player.c3.x >= 0.9 || Player.c4.x >= 0.9) {
            Player.c1.x = 0.7;
            Player.c2.x = 0.7;
            Player.c3.x = 0.9;
            Player.c4.x = 0.9;
        }
    } else if (direction == 3) {
        Player.c1.y += 0.1;
        Player.c2.y += 0.1;
        Player.c3.y += 0.1;
        Player.c4.y += 0.1;

        if (Player.c1.y >= 0.9 || Player.c4.y >= 0.9) {
            Player.c1.y = 0.9;
            Player.c2.y = 0.7;
            Player.c3.y = 0.7;
            Player.c4.y = 0.9;
        }
    }
    Player.renderPlayer();
    checkCollision();
}

void controller::checkCollision() {
    if(checkFloatEquality(Player.c1.x, Obstacle.c1.x) && checkFloatEquality(Player.c1.y, Obstacle.c1.y)) {
        Player.basket += Obstacle.weight;
        Obstacle.weight = 0;
        cout<<"Current basket: "<<Player.basket<<endl;
        cout<<"player capacity %: "<< (Player.basket/Player.capacity) * 100 <<endl;
    }
}

bool controller::checkFloatEquality(float x, float y) {
    float epsilon = 0.001f;
    return (fabs(x - y) < epsilon);
}

float controller::getForce() {

}
