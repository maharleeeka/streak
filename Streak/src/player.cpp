#include "player.h"
#include "shapes.h"
#include "coordinate.h"

#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */

#include <iostream>

coordinate::coordinate(){
    x = 0;
    y = 0;
}

coordinate::~coordinate(){
}

player::player(){
    //default
    c1.x = -0.1;
    c1.y = -0.7;
    c2.x = -0.1;
    c2.y = -0.9;
    c3.x = 0.1;
    c3.y = -0.9;
    c4.x = 0.1;
    c4.y = -0.7;

    basket = 0;
    capacity = rand() % 100 + 20;
    std::cout<<"player capacity: "<<capacity<<std::endl;
}

player::~player(){
    //dtor
}

void player::renderPlayer(){
    playerShape();
}

void player::playerShape(){
    glColor3f(123, 23, 1);
    shapes::drawPolygon1(this->c1, this->c2, this->c3, this->c4);
}

void player::setLife(float life){
    this->life = life;
}

void player::setPlayerPos(coordinate c1,coordinate c2,coordinate c3,coordinate c4){
    this->c1 = c1;
    this->c2 = c2;
    this->c3 = c3;
    this->c4 = c4;
}
