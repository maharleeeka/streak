#include "shapes.h"

shapes::shapes()
{
    //ctor
}

shapes::~shapes()
{
    //dtor
}

void shapes::drawColoredCircle(float px, float py, int loop, float a1, float a2)
{
    int i;

    glPushMatrix();
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        glEnable(GL_LINE_SMOOTH);
        glHint(GL_LINE_SMOOTH, GL_NICEST);

        glEnable(GL_POINT_SMOOTH);
        glHint(GL_POINT_SMOOTH, GL_NICEST);

        glBegin(GL_TRIANGLE_FAN);
            glVertex2f(px, py);

            for(i = 0; i < loop; i++)
            {
                float rad = i * DEG2RAD;
                glVertex2f((cos(rad) * a1) + px, (sin(rad) * a2) + py);
            }
        glEnd();
}

void shapes::drawEllipse(float px, float py, int loop, float a1, float a2)
{
    int i;

    glPushMatrix();
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        glEnable(GL_LINE_SMOOTH);
        glHint(GL_LINE_SMOOTH, GL_NICEST);

        glEnable(GL_POINT_SMOOTH);
        glHint(GL_POINT_SMOOTH, GL_NICEST);

        glBegin(GL_LINE_LOOP);

            for(i = 0; i < loop; i++)
            {
                float rad = i*DEG2RAD;
                glVertex2f((cos(rad) * a1) + px, (sin(rad) * a2) + py);
            }
        glEnd();
    glPopMatrix();
}

void shapes::drawPolygon1(coordinate c1, coordinate c2, coordinate c3, coordinate c4){

       glBegin(GL_QUADS);              // Each set of 4 vertices form a quad
          glVertex2f(c1.x, c1.y);    // x, y
          glVertex2f(c2.x, c2.y);
          glVertex2f(c3.x, c3.y);
          glVertex2f(c4.x, c4.y);
       glEnd();


       glFlush();  // Render now
}
