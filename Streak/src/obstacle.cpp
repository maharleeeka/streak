#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include "shapes.h"
#include "obstacle.h"
#include "coordinate.h"

#include <iostream>

using namespace std;

obstacle::obstacle(){
    float x1 = -0.1;
    float x2 = 0.1;
    float y = 0.9;
    c1.x = x1;
    c1.y = y;
    c2.x = x1;
    c2.y = y - 0.2;
    c3.x = x2;
    c3.y = y - 0.2;
    c4.x = x2;
    c4.y = y;

    this->weight = rand() % 20 + 1;
    std::cout<<"obstacle weight: "<<this->weight<<std::endl;
}

obstacle::~obstacle(){

}

void obstacle::renderObstacle(){
    drawObstacle();
}

void obstacle::drawObstacle() {
    glColor3f(0.2f, 0.6f, 0.0f);
    shapes::drawPolygon1(this->c1, this->c2, this->c3, this->c4);
    moveObstacle();
}

void setObstacleInitialPos() {
}

void obstacle::moveObstacle(){

}
