#include <windows.h>
#include <GL/glut.h>
#include <cstdlib>
#include <time.h>
#include <iostream>

#include "controller.h"

#define WINDOWWIDTH 600
#define WINDOWHEIGHT 600
#define WINDOWPOSX 200
#define WINDOWPOSY 50

char title[] = "Streak";
int refreshMillis = 100;

controller control;

GLdouble clipAreaXLeft, clipAreaXRight, clipAreaYBottom, clipAreaYTop;

void display();
void idle();
void initGL();
void key(unsigned char, int, int);
void mouse(int, int, int, int);
void resetKeys();
void reshape(GLsizei, GLsizei);
void specialKey(int, int, int);
void timer(int);

int main(int argc, char** argv){
    srand((unsigned)time(0));

    glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_MULTISAMPLE);
	glutInitWindowSize(WINDOWWIDTH, WINDOWHEIGHT);
	glutInitWindowPosition(WINDOWPOSX, WINDOWPOSY);


	glutCreateWindow(title);

	//Full screen mode
	//glutGameModeString("1024x600:32@60");
	//glutEnterGameMode();

    glEnable(GLUT_MULTISAMPLE);
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    //glutIdleFunc(idle);
    glutTimerFunc(0, timer, 0);
    glutSpecialFunc(specialKey);
    glutKeyboardFunc(key);
    glutMouseFunc(mouse);

    initGL();

    glutMainLoop();

	return 0;
}

void initGL(){
     glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
}

void display(){
    glClear(GL_COLOR_BUFFER_BIT);
    control.gameStart();
    glutSwapBuffers();
}

void idle(){
    glutPostRedisplay();
}

void key(unsigned char key, int x, int y){

}

void mouse(int button, int state, int x, int y)
{

}

void specialKey(int key, int x, int y){
    switch(key){
        case GLUT_KEY_RIGHT:
            control.movePlayer(2);
            break;
        case GLUT_KEY_LEFT:
            control.movePlayer(1);
            break;
        case GLUT_KEY_UP:
            control.movePlayer(3);
            break;
    }
}

/*void resetKeys(){
    int ctr;
    for(ctr = 0; ctr < 206; ctr++)
    {
        keys[ctr] = false;
    }
}*/

void reshape(GLsizei width, GLsizei height){
    if(height == 0)
        height = 1;

    GLfloat aspect = (GLfloat)width / (GLfloat)height;
    //std::cout<<"Aspect: "<<aspect<<std::endl;

    glViewport(0, 0, width, height);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    if(width >= height)
    {
        gluOrtho2D(-1.0 * aspect, 1.0 * aspect, -1.0, 1.0);
    }
    else
    {
        gluOrtho2D(-1.0, 1.0, -1.0 / aspect, 1.0 / aspect);
    }
}

void timer(int value){
    glutPostRedisplay();
    glutTimerFunc(refreshMillis, timer, 0);
}
